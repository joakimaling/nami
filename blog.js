const fs = require('fs')
const showdown = require('showdown')

const converter = new showdown.Converter()

fs.readdirSync('posts').forEach(file => {
	fs.readFile(`posts/${file}`, (error, data) => {
		console.log(converter.makeHtml(data.toString()));
	});
});

