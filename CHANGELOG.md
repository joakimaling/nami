## 0.1.1 (2020-05-05)
- Added Browsersync configuration file
- Added JavaScript bundler
- Fixed .editorconfig issues
- Minor additions/fixes
- Updated packages

## 0.1.0 (2019-09-22)
- Initial public release
