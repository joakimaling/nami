// Open external links in separate tabs/windows
document.querySelectorAll('a').forEach(tag => {
	if (tag.host !== location.host) {
		tag.setAttribute('rel', 'noopener noreferrer');
		tag.setAttribute('target', '_blank');
	}
});
