# Nami（波）

> Simple, blog-aware, static website builder

[![Licence][licence-badge]][licence-url]
[![Release][release-badge]][release-url]

Nami is a tool similar to [Jekyll](https://jekyllrb.com/) but uses JavaScript in
place of its Ruby and Liquid, and is configured using JSON instead of Yaml. Much
of my inspiration comes from Sanographix's excellent boilerplate [Rin 5][ringo].

## Installation

:bulb: Requires [Node.js][node-url].

Clone the project and install packages:

```sh
git clone https://gitlab.com/carolinealing/nami.git <your-project>
cd <your-project>
npm install
```

### Source 

Nami has a tree stuctrue shown below. Configuration is done by editing the files
`.babelrc`, `.jshintrc`, `.postcssrc`, `context.json` and `bs-config.js`.

```
nami
├─ejs/
│ ├─includes/
│ │ ├─footer.ejs # Bottom part of page
│ │ └─header.ejs # Top part of page
│ └─pages/
│   └─index.ejs
├─img/
├─js/
│ └─base.js
├─sass/
│ ├─_variables.sass # Customisable variables
│ └─base.sass
├─.babelrc     # Babel.js config
├─.jshintrc    # JSHint config
├─.postcssrc   # PostCSS config
├─bs-config.js # Browsersync config
├─context.json # EJS data
└─package.json
```

## Usage

To execute all the processes and watch folders for changes run:

```sh
npm start
```

### Build

The generated files are placed in `build` which has the following structure:

```
build/
├─css/
│ └─bundle.css
├─img/
│ └─...
├─js/
│ └─bundle.js
├─index.html
└─...
```

### Deploying to [GitHub pages](https://pages.github.com/)
TODO: Research deployment to GitLab Pages!

1. Remove the `build/` entry from `.gitignore`
2. Commit it: `git add build && git commit -m 'Initial commit of gh-pages'`
3. Make a separate branch: `git subtree push --prefix build origin gh-pages`

[Reference Gist](https://gist.github.com/cobyism/4730490)

## Versioning

Uses [Semantic Versioning](https://semver.org/). See [releases][release-url] and
[tags][tags-url] for versions of this project.

## Change-log

See [CHANGELOG.md](CHANGELOG.md) for a detailed list of changes.

## To-Do list

See [TODO.md](TODO.md) for upcoming changes.

## Licence

Released under MIT. See [LICENSE][licence-url] for more.

Coded with :heart: by [carolinealing][user-url].

[licence-badge]: https://badgen.net/gitlab/license/carolinealing/nami
[licence-url]: LICENSE
[node-url]: https://nodejs.org/en/download/
[release-badge]: https://badgen.net/gitlab/release/carolinealing/nami
[release-url]: https://gitlab.com/carolinealing/nami/-/releases
[ringo]: https://sanographix.github.io/rin/
[tags-url]: https://gitlab.com/carolinealing/nami/-/tags
[user-url]: https://gitlab.com/carolinealing
