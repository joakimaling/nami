# To-Do list

- [ ] Add blogging system
- [ ] Add extra files
	- [ ] feed.xml
	- [ ] humans.txt
	- [ ] robots.txt
	- [ ] sitemap.xml
- [ ] Add favicon
- [ ] Add the Qi project
- [ ] Build own cache buster
- [ ] Built-in variables/callbacks
- [ ] Fix: not always reloading when changing context.json
- [ ] Include Font Awesome
- [ ] Implement layouts
- [ ] Implement deployment script
- [ ] Put directories in "source" directory
